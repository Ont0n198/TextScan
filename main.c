#include <gtk/gtk.h>
#include <stdlib.h>
#include <stdio.h>
#include <gtk/gtkfilechooserbutton.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <err.h>
#include <time.h>
#include <math.h>

#include "Process/pixel_operations.h"
#include "Segmentation/segmentation_block.h"
#include "Neural_Network/nn.h"
#include "Neural_Network/matrix.h"
#include "Process/img_process.h"
#include "Train/train.h"
#include "Process/ecran.h"


static GtkWidget *text_view;
static struct NeuralNetwork *nn;

char* convert(char* img, char* train, int trainNum)
{
	SDL_Init(SDL_INIT_VIDEO);
	SDL_Surface *surf = load_image(img);

	image_to_gray_level(surf);
	int *bin = image_to_white_and_black(surf);
	struct List *text = segment(nn, bin, surf->w, surf->h, train);
	if(train == NULL)
	{
		FILE *converted = fopen("converted.txt", "w+");

		text = text->next;
		for(; text!=NULL; text = text->next)
			fprintf(converted, "%c", text->lettre);
		
		fclose(converted);
	}
	else
	{
		for(int i = 1; i<trainNum; i++)
		{	
			free(text);
			text = segment(nn, bin, surf->w, surf->h, train);
		}
	}
	free(text);
	free(bin);
	free(surf);
	SDL_Quit();
	return "converted.txt";
}

gchar* gtk_text_view_get_text ()
{
	GtkTextBuffer *buffer = 0; 
	GtkTextIter start; 
	GtkTextIter end;			
	gchar *buf = 0;
	
	buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(text_view));
	gtk_text_buffer_get_start_iter(buffer, &start);
	gtk_text_buffer_get_end_iter(buffer, &end);		
	buf = gtk_text_buffer_get_text(buffer, &start, &end, TRUE);
	return buf;
	g_free(buf);
}

void save_text()
{
	gchar *text = gtk_text_view_get_text();
	
	FILE *filem = fopen("converted.txt", "w+");
	fprintf(filem, "%s", text);
	fclose(filem);
}

gchar* get_way(gchar *filename)
{
	GtkWidget *dialog;
	gint res;
	
	dialog = gtk_file_chooser_dialog_new("Open file", NULL,
																				GTK_FILE_CHOOSER_ACTION_OPEN,
																				"_Convert",
																				GTK_RESPONSE_ACCEPT, NULL);
  res = gtk_dialog_run (GTK_DIALOG (dialog));
  if (res == GTK_RESPONSE_ACCEPT)
  {
  	GtkFileChooser *chooser = GTK_FILE_CHOOSER (dialog);
  	filename = gtk_file_chooser_get_filename (chooser);
  	filename = convert(filename, NULL, 0);
  } 
	gtk_widget_destroy(dialog);
  return filename;
}

void print_text(gchar *filename)
{
	GtkTextBuffer *buffer;
	FILE *fichier; 	
	GtkTextIter start;
	GtkTextIter end;
	gchar lecture[1024];
	
	filename = get_way(filename);
	buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(text_view));
	fichier = fopen(filename,"rt");
	gtk_text_buffer_get_start_iter(buffer, &start);
	gtk_text_buffer_get_end_iter(buffer, &end);
	gtk_text_buffer_delete(buffer, &start, &end);
	while(fgets(lecture, 1024, fichier))
	{
		gtk_text_buffer_get_end_iter(buffer, &end);
		gtk_text_buffer_insert(buffer , &end,
													 g_locale_to_utf8(lecture, -1,
													 NULL, NULL, NULL), -1);
	}
	fclose(fichier);
}

int window(int argc, char *argv[])
{
	GtkWidget *pWindow;
  GtkWidget *pVBox;
  GtkWidget *pHBox;
  GtkWidget *pButton[3];
  GtkWidget *scrollbar;
  char *filename = "";
  
	gtk_init(&argc, &argv);
	
	pWindow = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(pWindow), "TextScan");
	gtk_window_set_default_size(GTK_WINDOW(pWindow), 1000, 1000);
	
	
  pVBox = gtk_box_new(TRUE, 0);
  pHBox = gtk_box_new(FALSE, 0);
  
  gtk_container_add(GTK_CONTAINER(pWindow), pVBox);
  
  pButton[0] = gtk_button_new_with_label("    OCR    ");
  pButton[1] = gtk_button_new_with_label("     Save    ");
	pButton[2] = gtk_button_new_with_label("     Quit     ");
  text_view = gtk_text_view_new();
  scrollbar = gtk_scrolled_window_new(NULL, NULL);
	
	
  gtk_box_pack_start(GTK_BOX(pVBox), pHBox, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(pHBox), pButton[0], FALSE, FALSE, 0);
  gtk_box_pack_end(GTK_BOX(pHBox), pButton[2], FALSE, FALSE, 0);
  gtk_box_pack_end(GTK_BOX(pHBox), pButton[1], FALSE, FALSE, 10);
  gtk_box_pack_start(GTK_BOX(pVBox), scrollbar, TRUE, TRUE, 5);

  
  gtk_container_add(GTK_CONTAINER(scrollbar), text_view);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollbar),
																 GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

	g_signal_connect(G_OBJECT(pButton[0]), "clicked", G_CALLBACK(print_text),
														filename);
  g_signal_connect(G_OBJECT(pButton[2]), "clicked", G_CALLBACK(gtk_main_quit),
														NULL);
  g_signal_connect(G_OBJECT(pButton[1]), "clicked", G_CALLBACK(save_text),
														NULL);
  
  gtk_widget_show_all(pWindow);
	
	gtk_main();
	
	return 0;
}

void make_train(char *filename, char *img, int trainNb)
{
	FILE *f = fopen(filename, "r");
	char *text = malloc(sizeof(char)*10000);
	fscanf(f, "%s", text);
	fclose(f);

	convert(img, text, trainNb);
}

void make_trains(struct NeuralNetwork *nn, int nbTrains)
{
	double *inputs = malloc(sizeof(double)*2);
	double *targets = malloc(sizeof(double));
	int i = 0;
	while(i<nbTrains)
	{
		int r1 = rand()%2;
		int r2 = rand()%2;
		*(inputs+0) = r1;
		*(inputs+1) = r2;
		*(targets) = (double)(r1 + r2 == 1.);
		train(nn, inputs, targets);
		i++;
	}
	free(inputs);
	free(targets);
}

void print_mat(double *mat, int rows, int cols)
{
	printf("============\n");
	for(int i = 0; i < rows; i++)
	{
		for(int j = 0; j < cols; j++)
			printf("%f ", *(mat + i*cols + j));
		printf("\n");
	}
	printf("============\n");
}

int main(int argc, char *argv[])
{
	srand(time(NULL));
	
	if(argc > 1 && *argv[1] == 'x')
	{
	  struct NeuralNetwork *nn = newNN(2, 20, 1, 0.05);

		double *input = malloc(sizeof(double)*2);
		
		if(argc == 3 && *argv[2] == 't')
			make_trains(nn, 100000);
		//save(nn);

		*(input+0) = 0.;
		*(input+1) = 0.;
		double *output = query(nn, input);
		int b = *output < 0.5;
		print_mat(output, 1, 1);
		free(output);

		*(input+0) = 0.;
		*(input+1) = 1.;
		output = query(nn, input);
		b &= *output > 0.5;
		print_mat(output, 1, 1);
		free(output);

		*(input+0) = 1.;
		*(input+1) = 0.;
		output = query(nn, input);
		b &= *output > 0.5;
		print_mat(output, 1, 1);
		free(output);

		*(input+0) = 1.;
		*(input+1) = 1.;
		output = query(nn, input);
		b &= *output < 0.5;
		print_mat(output, 1, 1);
		free(output);

		free(input);
		free(nn);
		printf("%d\n", b);

    return 1;
	}

	int r = 0;
	int t = 0;
	char *pathCorr = "Train/Text.corr";
	char *pathImg = "img/Text.jpg";
	if(argc == 1)
		r = 1;
	else
	{
		r = *argv[1] == 'r';
		if(argc == 3)
			t = atoi(argv[2]);
		if(argc == 5)
		{
			pathCorr = argv[3];
			pathImg = argv[4];
			t = atoi(argv[2]);
		}
	}
	
	if(r)
		nn = readNet();
	else
		nn = newNN(1024, 700, 69, 0.05);//nbletters = 69 img= 32*32
	if(t != 0)
		make_train(pathCorr, pathImg, t);
	save(nn);
	
	window(argc, argv);
	
	free(nn->who);
	free(nn->wih);
	free(nn);
  return 1;
}

