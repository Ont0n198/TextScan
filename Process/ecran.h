# ifndef ECRAN_H_
# define ECRAN_H_

#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <err.h>

SDL_Surface* load_image(char *path);

#endif
