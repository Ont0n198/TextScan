# ifndef IMG_PROCESS_H_
# define IMG_PROCESS_H_

#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <err.h>
#include "pixel_operations.h"
//#include "ecran.h"

SDL_Surface* image_to_gray_level(SDL_Surface *img);

int* image_to_white_and_black(SDL_Surface *img);

void histogram(SDL_Surface* img, int histo[]);

SDL_Surface* remove_grain(SDL_Surface* img);

SDL_Surface* mult_img(SDL_Surface* img, double multW, double multH);

#endif
