#ifndef _MATRIX_H_
#define _MATRIX_H_

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double* mat(int rows, int cols);

#define PI 3.141592654
double gaussrand();

void randomize_mat(double *mat, int rows, int cols);

double* transpose_mat(double *mat, int rows, int cols);

double* copy_mat(double *mat, int rows, int cols);

void add_mat_mat(double *mat, int rows, int cols, double *add);

void add_mat_single(double *mat, int rows, int cols, double single);

void mult_mat_mat(double *mat, int rows, int cols, double *mul);

void mult_mat_single(double *mat, int rows, int cols, double single);

void map_mat(double *mat, int cols, int rows, double (*fonc)(double));

double* subtract_mat(double *mat, int rows, int cols, double *sub);

double* dot_mat(double *mat1, double *mat2, int rows, int cols, int cols2);

#endif
