#ifndef _SEGMENTATION_BLOCK_H_
#define _SEGMENTATION_BLOCK_H_

#include <stdlib.h>
#include <stdio.h>
#include <SDL.h>
#include <SDL_image.h>

#include "../Process/pixel_operations.h"
#include "../Neural_Network/nn.h"


struct matrix_image
{
	int *array; 
	int w;
	int h;
	
	int size;
};

struct Line
{
  int from;
  int to;
  int skipped;
	int jump;
};

struct Lines
{
  struct Line **lines;
  int nbLines;
  int sizeMoy;
};

struct List
{
	struct List *next;
	char lettre;
};

struct Lines* init_lines(int size);

struct Line* init_line(int from, int to);

struct List* segment(struct NeuralNetwork *nn, int *bin, int w, int h,
										 char *t);

void separateLines(struct matrix_image *img, struct Lines *ls);
                   
struct List* separateChar(struct NeuralNetwork *nn, struct matrix_image *img,
													struct Lines *ls,
													char *train);

struct matrix_image* matrix_create_from_image(int *bin, int w, int h);

void lines_free(struct Lines *lines);

void list_free(struct List *list);

int matrix_distance_horizontal(struct matrix_image *img, int x_from);

int matrix_distance_vertical(struct matrix_image *img, int y_from);

struct matrix_image* matrix_copy(struct matrix_image *img);

struct List* init_list(char ch);

char returnChar(struct NeuralNetwork *nn, struct matrix_image *img,
								int x, int y, int w, int h);

#endif

